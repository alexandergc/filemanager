## FileManagerServer
This project is to complement with FileManagerWebApp.
This project must be ran first before FileManagerWebApp.

## Installation
1. You may skip this section if you have already installed Node.js
2. Download [NodeJS](https://nodejs.org/en/download/)
3. Once installer finishes download, launch it and follow the wizard to install Node.js

## Setup environment variables
1. Check if .env file is available in the working directory.
2. If file is not available, create a `.env` file in the working directory.
3. You may edit the APP_PORT to your liking.

```
# APP
APP_HOST=localhost
APP_PORT=3000
API_VERSION=v1

# MONGO DB CONNECTION
DB_MONGO_DB=file_manager
DB_MONGO_USERNAME=admin 
DB_MONGO_PASSWORD=p@ss

# JWT
ACCESS_TOKEN_SECRET=secret_token
```

## How to start
1. Go to the working directory
2. Run `npm install` (to install dependencies)
3. Once dependencies have been installed, run `npm start` to run application on `http://localhost:3000/`
4. Once server is app and running, you may proceed to test the FileManagerWebApp.

## Check file uploads
1. File uploads will be located in the working directory `uploads` folder
