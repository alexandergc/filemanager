var express = require('express');
var router = express.Router();
var authController = require('../controllers/AuthenticationController');

router.route('/login').post(authController.login);

module.exports = router;