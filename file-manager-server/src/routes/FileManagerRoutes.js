const express = require('express');
const router = express.Router();
const jwt = require('../utils/jwtUtil');

const fileManagerController = require('../controllers/FileManagerController');

const mongoose = require('mongoose');
const FileSchema = require('../models/FilesDetails');
const FileTable = mongoose.model('filesDetails', FileSchema);

const multer  = require('multer')
const storage = multer.diskStorage({
    destination: 'uploads/',
    filename: function (req, file, cb) {
        cb(null, req.body.userId + '_' + Date.now() + '_' + file.originalname);
    }
  });
const upload = multer({ storage: storage });

router.route('/file-handler').get(jwt.authenticateJWT, fileManagerController.getFiles);
router.route('/file-handler').delete(jwt.authenticateJWT, fileManagerController.deleteFile);

router.route('/file-handler/upload').post(jwt.authenticateJWT, upload.single('file'), (req, res) => {
    try {
        const file = req.file;
        const fileDetails = {
            userId: req.body.userId,
            title: req.body.title,
            description: req.body.description,
            tags: JSON.parse(req.body.tags),
            filename: file.originalname,
            mimetype: file.mimetype,
            path: file.path
        };
        const newFile = new FileTable(fileDetails);
        newFile.save( (err, result) => {
            if (err) {
                res.sendStatus(500);
            } else {
                res.status(200).json(result);
            }
        });
        
    } catch (err) {
        res.sendStatus(500);
    }
});

module.exports = router;