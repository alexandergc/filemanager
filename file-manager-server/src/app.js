const express = require('express');
const http = require("http");
const dotenv = require('dotenv').config();
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.APP_PORT || 3000;

// IMPORT ROUTES
const authenticationRoutes = require('./routes/AuthenticationRoutes');
const fileManagerRoutes = require('./routes/FileManagerRoutes');


// CONNECT TO DB
const mongoose = require('mongoose');
const uri = `mongodb+srv://${process.env.DB_MONGO_USERNAME}:${process.env.DB_MONGO_PASSWORD}@cluster0.n3fbb.mongodb.net/${process.env.DB_MONGO_DB}?retryWrites=true&w=majority`;
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
.then(() => { console.log("MONGO DB Connected")})
.catch(err => console.log(err))

// ENABLE CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

// SET BODY PARSERS
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true }));
app.use(bodyParser.raw( { type: 'application/json' } ));

// SET ROUTES
const API_PREFIX = `/api/${process.env.API_VERSION}`;
app.use(API_PREFIX, authenticationRoutes);
app.use(API_PREFIX, fileManagerRoutes);

const jwt = require('./utils/jwtUtil');
// app.use(API_PREFIX + '/uploads', jwt.authenticateJWT, express.static('uploads'));
app.use(API_PREFIX + '/uploads', express.static('uploads'));

http.createServer(app).listen(PORT, ()=> {
    console.log(`Server is running on ${PORT}`);
});