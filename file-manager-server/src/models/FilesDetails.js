const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = Schema.Types;

const FileSchema = new Schema({
    userId: Types.String,
    filename: Types.String,
    mimetype: Types.String,
    path: Types.String,
    title: Types.String,
    description: Types.String,
    tags: Types.Array
});

module.exports = FileSchema;