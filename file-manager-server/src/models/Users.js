const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = Schema.Types;

const UserSchema = new Schema({
    _id: Types.String,
    username: Types.String,
    password: Types.String
});

module.exports = UserSchema;