const mongoose = require('mongoose');
const FileSchema = require('../models/FilesDetails');
const FileTable = mongoose.model('filesDetails', FileSchema);

const fs = require('fs');

const FileManagerController = {
    async getFiles (req, res) {
        try {
            const userId = req.user.id;

            await FileTable.find( { userId: userId }, (err, details) => {
                if (err) {
                    res.sendStatus(500);
                } else {
                    if (details) {
                        res.status(200).json(details);
                    }
                    else {
                        res.status(200).json([]);
                    }
                }
            });
        }
        catch (err) {
            res.sendStatus(500);
        }
    },
    async deleteFile (req, res) {
        const id = req.query.id;
        const path = req.query.path;

        try {
            await FileTable.deleteOne({ _id: id }, (err, details) => {
                if (err) {
                    res.sendStatus(500);
                } else {
                    fs.unlink(path, () => {
                        res.status(200).json();
                        console.log('File Deleted.');
                    });
                }
            });
        }
        catch (err) {
            res.sendStatus(500);
        }
    }
};

module.exports = FileManagerController;