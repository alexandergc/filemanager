const mongoose = require('mongoose');
const UserSchema = require('../models/Users');
const UserTable = mongoose.model('users', UserSchema);

const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();
const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET;

const AuthenticationController = {
    async login (req, res) {
        console.log(req.body);
        const username = req.body.username;
        const password = req.body.password
        
        try {
            await UserTable.findOne( { username: username }, (err, user) => {
                if (user && user.password == password) {
                    const accessToken = jwt.sign({ id: user._id, username: user.username }, accessTokenSecret);
                    res.status(200).json(accessToken);
                } else {
                    console.log('error', err);
                    res.sendStatus(401);
                }
            });
        } catch (err) {
            console.log('error', err);
            res.sendStatus(500);
        }
        
    }
};

module.exports = AuthenticationController;