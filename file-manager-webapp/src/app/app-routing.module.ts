import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './common/components/layout/layout.component';

import { AuthGuard } from './features/authentication/guards/auth.guard';
import { LoginComponent } from './features/authentication/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'home',
    component: LayoutComponent,
    loadChildren: './features/files-management/file-management.module#FileManagementModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
