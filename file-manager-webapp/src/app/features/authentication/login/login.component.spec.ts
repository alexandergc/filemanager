import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not submit request when username and password is empty', () => {
    const form = component.LoginForm;

    const usernameInput = form.get('Username');
    usernameInput.setValue('');

    const passwordInput = form.get('Password');
    passwordInput.setValue('');

    component.login();

    expect(form.valid).toBeFalse();
    expect(form.hasError('unauthorized')).toBeTrue();
  });

  it('should not submit request when username is empty', () => {
    const form = component.LoginForm;

    const usernameInput = form.get('Username');
    usernameInput.setValue('');

    const passwordInput = form.get('Password');
    passwordInput.setValue('test');

    component.login();

    expect(form.valid).toBeFalse();
    expect(form.hasError('unauthorized')).toBeTrue();
  });

  it('should not submit request when password is empty', () => {
    const form = component.LoginForm;

    const usernameInput = form.get('Username');
    usernameInput.setValue('user');

    const passwordInput = form.get('Password');
    passwordInput.setValue('');

    component.login();

    expect(form.valid).toBeFalse();
    expect(form.hasError('unauthorized')).toBeTrue();
  });

  it('form should be valid when username and password has values', () => {
    const form = component.LoginForm;

    const usernameInput = form.get('Username');
    usernameInput.setValue('user');

    const passwordInput = form.get('Password');
    passwordInput.setValue('test');

    component.login();

    expect(form.valid).toBeTrue();
    expect(form.hasError('unauthorized')).toBeFalse();
  });
});
