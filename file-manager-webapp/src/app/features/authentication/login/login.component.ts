import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';

import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  isProcessingLogin: boolean;
  maskPassword = true;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['home']);
    }
  }

  ngOnInit() {
    this.LoginForm = this.fb.group({
      Username: ['', Validators.required],
      Password: ['', Validators.required]
    });
  }

  public login() {
    const form = this.LoginForm.value;

    if (form.Username && form.Password) {
      this.isProcessingLogin = true;
      const credentials = {
        username: form.Username,
        password: Md5.hashStr(form.Password)
      };

      this.authService.login(credentials).subscribe(
        (authResult) => {
          this.isProcessingLogin = false;
          this.authService.setSession(authResult);
          this.router.navigate(['home']);
        },
        error => {
          this.isProcessingLogin = false;
          this.handleLoginError();
        }
      );
    } else {
      this.handleLoginError();
    }
  }

  private handleLoginError() {
    this.LoginForm.setErrors({ unauthorized : true });
  }

}
