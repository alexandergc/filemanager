import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';

import { routes } from 'src/environments/environment';
import { UserDetails } from '../models/UserDetails';
import { RequestService } from 'src/app/common/services/http-request/request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  user = new UserDetails();

  constructor(
    private router: Router,
    private requestService: RequestService
  ) { }

  public login(credentials) {
    const url = routes.auth.login;
    return this.requestService.post(url, credentials);
  }

  public logout() {
    this.user = new UserDetails();
    this.clearToken();
    this.router.navigate(['']);
  }

  public isLoggedIn() {
    return this.getSession();
  }

  public setSession(authResult) {
    sessionStorage.setItem('token', authResult);
    const token = this.getDecodedToken();
    this.setUser(token);
  }

  private setUser(token) {
    if (token) {
      this.user.id = token.id;
      this.user.username = token.username;
    }
  }

  private getSession() {
    const token = this.getDecodedToken();
    if (token) {
      this.setUser(token);
      return true;
    }
    return false;
  }

  private getDecodedToken() {
    const token = sessionStorage.getItem('token');
    if (token) {
      try {
        return jwt_decode(token);
      } catch (error) {
        this.clearToken();
        return null;
      }
    }
    return null;
  }

  public getUser() {
    return this.user;
  }

  private clearToken() {
    sessionStorage.removeItem('token');
  }
}
