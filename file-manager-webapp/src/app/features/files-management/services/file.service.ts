import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/common/services/http-request/request.service';
import { routes } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(
    private requestService: RequestService
  ) { }

  retrieveFiles() {
    const url = routes.fileHandle.root;
    return this.requestService.get(url);
  }

  upload(details) {
    const url = routes.fileHandle.upload;
    let formData = new FormData();
    formData.append('userId', details.userId);
    formData.append('file', details.file);
    formData.append('title', details.title);
    formData.append('description', details.description);
    formData.append('tags', JSON.stringify(details.tags));

    return this.requestService.post(url, formData);
  }

  deleteFile(file) {
    const params = {
      id: file._id,
      path: file.path
    };
    const url = routes.fileHandle.root;
    return this.requestService.delete(url, params);
  }
}
