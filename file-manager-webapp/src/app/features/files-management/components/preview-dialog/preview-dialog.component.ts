import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { routes } from 'src/environments/environment';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-preview-dialog',
  templateUrl: './preview-dialog.component.html',
  styleUrls: ['./preview-dialog.component.css']
})
export class PreviewDialogComponent implements OnInit {
  @ViewChild('videoPlayer') videoplayer;
  file;
  videoplayerWidth = 800;
  innerWidth;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<PreviewDialogComponent>
  ) { }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.innerWidth = window.innerWidth;
    
    if (this.innerWidth > 1050) {
      this.videoplayerWidth = 1000;
    } else if (this.innerWidth <= 1050) {
      this.videoplayerWidth = 600;
    }
  }

  ngOnInit(): void {
    this.file = this.data.file;
    this.innerWidth = window.innerWidth;
  }

  public isImage() {
    return this.file.mimetype.toLowerCase().includes('jpg') || this.file.mimetype.toLowerCase().includes('jpeg');
  }

  public isVideo() {
    return this.file.mimetype.toLowerCase().includes('mp4')
  }

  public getFileName() {
    return this.file.filename;
  }

  public getFilePath() {
    return routes.server + '/' + this.file.path;
  }

  public toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
}

  public close() {
    this.dialogRef.close();
  }
}
