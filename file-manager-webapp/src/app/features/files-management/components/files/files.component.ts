import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { FileService } from '../../services/file.service';
import { FileUploadDialogComponent } from '../file-upload-dialog/file-upload-dialog.component';
import { PreviewDialogComponent } from '../preview-dialog/preview-dialog.component';

import { routes } from 'src/environments/environment';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {
  files = [];

  constructor(
    public dialog: MatDialog,
    private snackbar: MatSnackBar,
    private fileService: FileService
  ) { }

  ngOnInit(): void {
    this.retrieveFiles();
  }

  public retrieveFiles() {
    this.fileService.retrieveFiles().subscribe(
      res => {
        this.files = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  public showTags(tags) {
    let tagStr = '';
    for (let i = 0; i < tags.length; i++) {
      tagStr += tags[i].name;

      if (i != tags.length-1) {
        tagStr += ', ';
      }
    }
    return tagStr;
  }

  public openUploadDialog() {
    const dialogRef = this.dialog.open(FileUploadDialogComponent, {
      autoFocus: false,
      disableClose: true,
      height: '420px',
      width: '600px',
    });

    dialogRef.afterClosed().subscribe(isOK => {
      if (isOK) {
        this.retrieveFiles();
      }
    });
  }

  public preview(file) {
    if (file.mimetype.includes('pdf')) {
      window.open(this.getFilePath(file), '_blank');
    } else {
      this.dialog.open(PreviewDialogComponent, {
        autoFocus: false,
        disableClose: true,
        data: {
          file
        }
      }); 
    }
  }

  public deleteFile(file) {
    this.fileService.deleteFile(file).subscribe(
      res => {
        this.retrieveFiles();

        const message = 'File has been deleted.';
        this.snackbar.open(message, 'close', {
          duration: 3500,
        });
      },
      err => {
        const message = 'Oops. Server error.';
        this.snackbar.open(message, 'close', {
          duration: 3500,
        });
      }
    );
  }

  public getFilePath(file) {
    return routes.server + '/' + file.path;
  }

}
