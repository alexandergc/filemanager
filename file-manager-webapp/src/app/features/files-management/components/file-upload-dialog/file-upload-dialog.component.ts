import { Component, OnInit, ViewChild } from '@angular/core';
import { COMMA, ENTER, SEMICOLON, SPACE } from '@angular/cdk/keycodes';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AuthenticationService } from 'src/app/features/authentication/services/authentication.service';
import { FileService } from '../../services/file.service';

@Component({
  selector: 'app-file-upload-dialog',
  templateUrl: './file-upload-dialog.component.html',
  styleUrls: ['./file-upload-dialog.component.css']
})
export class FileUploadDialogComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  FileUploadForm: FormGroup;
  isProcessing = false;
  hasSelectedFile = false;

  tags = [];
  removableTag = true;
  addOnBlur = true;
  hasReachedLimit = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON, SPACE];
  
  chipMessage = '';
  MAX_TAGS = 5;
  ADD_TAG_MSG = 'Add a tag..';
  CANNOT_ADD_TAG_MSG = 'Cannot add more than ' + this.MAX_TAGS + ' tags';

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private dialogRef: MatDialogRef<FileUploadDialogComponent>,
    private snackbar: MatSnackBar,
    private fileService: FileService
  ) { }

  ngOnInit(): void {
    const user = this.authService.getUser();

    this.FileUploadForm = this.fb.group({
      title: [''],
      description: [''],
      file: [],
      userId: [user.id]
    });

    this.chipMessage = this.ADD_TAG_MSG;
  }

  selectFile() {
    this.fileInput.nativeElement.click();
  }

  fileChange(files: File[]) {
    if(files.length > 0) {
      if (this.isSupportedFileType(files[0])) {
        this.hasSelectedFile = true;
        this.FileUploadForm.get('file').setValue(files[0]);
      } else {
        const message = 'File is not supported. Please select a supported file format.';
        this.snackbar.open(message, 'close', {
          duration: 3500,
        });
      }
    }
  }

  getFileName() {
    const file = this.FileUploadForm.get('file').value;

    if (file) {
      return file.name;
    }
    return 'Please select a file.';
  }

  isSupportedFileType(file) {
    return this.isJpg(file) || this.isMP4(file) || this.isPDF(file);
  }

  private isJpg(file) {
    const type = file.type.toLowerCase();
    return type.includes('jpg') || type.includes('jpeg');
  }

  private isMP4(file) {
    const type = file.type;
    return type.includes('mp4');
  }

  private isPDF(file) {
    const type = file.type.toLowerCase();
    return type.includes('pdf');
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push({name: value.trim()});
    }

    if (input) {
      input.value = '';
    }

    this.checkTagLimit();
  }

  removeTag(tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
    this.checkTagLimit();
  }

  private checkTagLimit() {
    if (this.tags.length === this.MAX_TAGS) {
      this.hasReachedLimit = true;
      this.chipMessage = this.CANNOT_ADD_TAG_MSG;
    } else {
      this.hasReachedLimit = false;
      this.chipMessage = this.ADD_TAG_MSG;
    }
  }

  upload() {
    if (this.FileUploadForm.valid) {
      const form = this.FileUploadForm.value;
      form.tags = this.tags;

      this.isProcessing = true;
      this.fileService.upload(form).subscribe(
        res => {
          const message = 'Upload complete.';
          this.snackbar.open(message, 'close', {
            duration: 3500,
          });
          this.isProcessing = false;
          this.dialogRef.close(res);
        },
        err => {
          this.isProcessing = false;
          const message = 'Failed to upload.';
          this.snackbar.open(message, 'close', {
            duration: 3500,
          });
        }
      );
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}
