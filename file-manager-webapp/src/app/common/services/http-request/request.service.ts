import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { headers, routes } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private httpOptionsJson = {
    headers: new HttpHeaders(headers.httpHeadersJson)
  };

  constructor(
    private http: HttpClient,

  ) { }

  get(url: string, ...params): Observable<any> {
    let completeURL =  routes.server + url;

    if (params.length !== 0) {
      for (const paramObj of params) {
        completeURL += '?';
        const paramKeys = Object.keys(paramObj);
        for (let i = 0; i < paramKeys.length; i++) {
          if (i === 0) {
            completeURL += '';
          } else {
            completeURL += '&';
          }
          completeURL += paramKeys[i] + '=' + paramObj[ paramKeys[i] ];
        }
      }
    }
    return this.http.get<any>(completeURL);
  }

  post(url: string, body: any): Observable<any> {
    const completeURL = routes.server + url;
    return this.http.post<any>(completeURL, body, this.httpOptionsJson);
  }

  delete(url: string, ...params): Observable<any> {
    let completeURL =  routes.server + url;

    if (params.length !== 0) {
      for (const paramObj of params) {
        completeURL += '?';
        const paramKeys = Object.keys(paramObj);
        for (let i = 0; i < paramKeys.length; i++) {
          if (i === 0) {
            completeURL += '';
          } else {
            completeURL += '&';
          }
          completeURL += paramKeys[i] + '=' + paramObj[ paramKeys[i] ];
        }
      }
    }
    return this.http.delete<any>(completeURL);
  }
}
