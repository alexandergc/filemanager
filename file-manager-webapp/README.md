## FileManagerWebApp
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0.

## Installation
1. You may skip this section if you have already installed Node.js
2. Download [NodeJS](https://nodejs.org/en/download/)
3. Once installer finishes download, launch it and follow the wizard to install Node.js

## How to start
1. Go to the working directory
2. Run `npm install` (to install dependencies)
3. Once dependencies have been installed, run `npm start` to run application on `http://localhost:4200/`
4. Navigate to `http://localhost:4200/` to open web application.

## Users
Below are accounts available for use:
1. Username: user1, Password: 1
2. Username: user2, Password: 1

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
