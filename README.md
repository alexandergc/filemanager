# FileManager

## Installation
1. You may skip this section if you have already installed Node.js
2. Download [NodeJS](https://nodejs.org/en/download/)
3. Once installer finishes download, launch it and follow the wizard to install Node.js

## How to start
1. Navigate to file-manager-web-app and check README file for instructions
2. Navigate to file-manager-server and check README file for instructions

## Users
Below are accounts available for use:
1. Username: user1, Password: 1
2. Username: user2, Password: 1
